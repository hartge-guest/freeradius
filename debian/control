Source: freeradius
Build-Depends: debhelper (>= 10.6),
 quilt,
 dpkg-dev (>= 1.13.19),
 autotools-dev,
 libcurl4-openssl-dev | libcurl4-gnutls-dev,
 libcap-dev,
 libgdbm-dev,
 libiodbc2-dev,
 libjson-c-dev,
 libkrb5-dev | heimdal-dev,
 libldap2-dev,
 libpam0g-dev,
 libpcap-dev,
 libperl-dev,
 default-libmysqlclient-dev | libmysqlclient-dev,
 libpq-dev,
 libreadline-dev,
 libsasl2-dev,
 libsqlite3-dev,
 libssl-dev,
 libsystemd-dev,
 libtalloc-dev,
 libwbclient-dev,
 libyubikey-dev,
 libykclient-dev,
 libmemcached-dev,
 libhiredis-dev,
 python3-dev,
 samba-dev | samba4-dev,
 libpcre3-dev,
 libcollectdclient-dev,
 snmp,
 dh-autoreconf,
 freetds-dev
Section: net
Priority: optional
Maintainer: Debian FreeRADIUS Packaging Team <pkg-freeradius-maintainers@lists.alioth.debian.org>
Uploaders: Mark Hymers <mhy@debian.org>,
 Sam Hartman <hartmans@debian.org>,
 Bernhard Schmidt <berni@debian.org>
Standards-Version: 4.4.1
Homepage: http://www.freeradius.org/
Vcs-Git: https://salsa.debian.org/debian/freeradius.git
Vcs-Browser: https://salsa.debian.org/debian/freeradius

Package: freeradius
Architecture: any
Depends: lsb-base (>= 3.1-23.2), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}, freeradius-common, freeradius-config, libfreeradius3 (= ${binary:Version})
Provides: radius-server
Recommends: freeradius-utils
Suggests: freeradius-ldap, freeradius-postgresql, freeradius-mysql, freeradius-krb5, snmp, freeradius-python3
Description: high-performance and highly configurable RADIUS server
 FreeRADIUS is a high-performance RADIUS server with support for:
  - Authentication by local files, SQL, Kerberos, LDAP, PAM, and more.
  - Powerful policy configuration language.
  - Proxying and replicating requests by any criteria.
  - Support for many EAP types; TLS, PEAP, TTLS, etc.
  - Many vendor-specific attributes.
  - Regexp matching in string attributes.
 and lots more.

Package: freeradius-common
Depends: adduser, ${misc:Depends}
Architecture: all
Conflicts: radiusd-livingston, xtradius, yardradius
Replaces: freeradius (<< 3)
Description: FreeRADIUS common files
 This package contains common files used by several of the other packages from
 the FreeRADIUS project.

Package: freeradius-config
Architecture: any
Depends: freeradius-common (>= 3), ${misc:Depends}, openssl, make, ca-certificates, ssl-cert, adduser
Breaks: freeradius-config, freeradius (<< 3.0.11+dfsg-1)
Replaces: freeradius (<< 3.0.11+dfsg-1)
Description: FreeRADIUS default config files
 freeradius-config contains the default configuration for FreeRADIUS.
 .
 You can install a custom package which sets "Provides: freeradius-config" in
 order to use the FreeRADIUS packages without any default configuration getting
 into your way.

Package: freeradius-utils
Architecture: any
Replaces: freeradius (<< 3)
Conflicts: radiusd-livingston, yardradius
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}, freeradius-common, freeradius-config, libfreeradius3 (= ${binary:Version})
Recommends: libdbi-perl
Description: FreeRADIUS client utilities
 This package contains various client programs and utilities from
 the FreeRADIUS Server project, including:
  - radclient
  - radeapclient
  - radlast
  - radsniff
  - radsqlrelay
  - radtest
  - radwho
  - radzap
  - rlm_ippool_tool
  - smbencrypt

Package: libfreeradius3
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Breaks: libfreeradius-dev (<< 3.0.11+dfsg-1)
Replaces: libfreeradius-dev (<< 3.0.11+dfsg-1)
Description: FreeRADIUS shared library
 The FreeRADIUS projects' libfreeradius-radius and libfreeradius-eap, used by
 the FreeRADIUS server and some of the utilities.

Package: libfreeradius-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}, libfreeradius3 (= ${binary:Version}), freeradius-dhcp (= ${binary:Version})
Description: FreeRADIUS shared library development files
 The FreeRADIUS projects' libfreeradius-radius and libfreeradius-eap, used by
 the FreeRADIUS server and some of the utilities.
 .
 This package contains the development headers and static library version.

Package: freeradius-dhcp
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: DHCP module for FreeRADIUS server
 The FreeRADIUS server can act as a DHCP server, and this module
 is necessary for that.

Package: freeradius-krb5
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: kerberos module for FreeRADIUS server
 The FreeRADIUS server can use Kerberos to authenticate users, and this module
 is necessary for that.

Package: freeradius-ldap
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: LDAP module for FreeRADIUS server
 The FreeRADIUS server can use LDAP to authenticate users, and this module
 is necessary for that.

Package: freeradius-rest
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: REST module for FreeRADIUS server
 The FreeRADIUS server can make calls to remote web APIs, and this module
 is necessary for that.

Package: freeradius-postgresql
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: PostgreSQL module for FreeRADIUS server
 The FreeRADIUS server can use PostgreSQL to authenticate users and do
 accounting, and this module is necessary for that.

Package: freeradius-mysql
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: MySQL module for FreeRADIUS server
 The FreeRADIUS server can use MySQL to authenticate users and do accounting,
 and this module is necessary for that.

Package: freeradius-iodbc
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: iODBC module for FreeRADIUS server
 The FreeRADIUS server can use iODBC to access databases to authenticate users
 and do accounting, and this module is necessary for that.

Package: freeradius-redis
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: Redis module for FreeRADIUS server
 This module is required to enable the FreeRADIUS server to access
 Redis databases.

Package: freeradius-memcached
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: Memcached module for FreeRADIUS server
 The FreeRADIUS server can cache data in memcached and this package
 contains the required module.

Package: freeradius-yubikey
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Description: Yubikey module for FreeRADIUS server
 This package is required to add Yubikey functionality to the
 FreeRADIUS server.

Package: freeradius-python3
Architecture: any
Depends: freeradius (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}
Breaks: freeradius (<< 3.0.16+dfsg-4~)
Replaces: freeradius (<< 3.0.16+dfsg-4~)
Description: Python 3 module for FreeRADIUS server
 This package is required to add Python 3 functionality to the
 FreeRADIUS server.
 .
 It was introduced in FreeRADIUS 3.0.20 as EXPERIMENTAL module. Use at
 your own risk.
